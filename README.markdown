# Slice Samples, Shown Succinctly

This Android project contains a few modules that demonstrate the APIs for
slices, in support of [an Android Summit 2018 conference presentation](http://androidsummit.org).

The `sampler` module contains an app that publishes a slice that shows
off a few types of slice rows.

The `slice-and-dice` module contains an app that publishes a slice that
shows how to push updates to the slice content, in this case "re-rolling"
a set of dice.

The `inspector` module contains an app that can render a slice, in
shortcut, small, and large modes. It also "renders" the slice by pouring
the slice data into a graph showing the various nodes (though due to
limitations in the graph library, most of the nodes will draw off-screen).
You configure which slice is "inspected" by updating the
`SLICE_URI` property in the `gradle.properties` file in the project
root directory, then rebuilding the app.

All three projects are written in Kotlin and use alpha/beta editions
of the `androidx` libraries.

The code for this project is released under the Apache Software License 2.0.

To learn more about slices and other areas of Android app development,
get [CommonsWare's books](https://commonsware.com).
