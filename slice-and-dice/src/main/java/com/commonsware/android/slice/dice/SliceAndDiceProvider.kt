/***
 * Copyright (c) 2018 CommonsWare, LLC
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain	a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 * by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * Covered in detail in the book _The Busy Coder's Guide to Android Development_
 * https://commonsware.com/Android
 */

package com.commonsware.android.slice.dice

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.core.graphics.drawable.IconCompat
import androidx.slice.Slice
import androidx.slice.SliceProvider
import androidx.slice.builders.*
import androidx.slice.builders.ListBuilder.SMALL_IMAGE

class SliceAndDiceProvider : SliceProvider() {
  override fun onCreateSliceProvider() = true

  override fun onBindSlice(sliceUri: Uri): Slice? {
    val ctxt = context ?: return null

    return list(ctxt, sliceUri, ListBuilder.INFINITY) {
      setAccentColor(ctxt.resources.getColor(R.color.colorAccent))
      addAction(buildRollAction(ctxt))

      header {
        title = ctxt.getString(R.string.header_title)
        primaryAction = buildRollAction(ctxt)
      }

      gridRow {
        for (i in 1..5) {
          val die = FACES[(Math.random() * 6).toInt()]

          val bmp = BitmapFactory.decodeResource(ctxt.resources, die)

          cell {
            addImage(IconCompat.createWithBitmap(bmp), SMALL_IMAGE)
          }
        }
      }
    }
  }

  private fun buildRollAction(ctxt: Context): SliceAction {
    val bmp = BitmapFactory.decodeResource(ctxt.resources,
        R.drawable.refresh_18dp)

    return SliceAction.create(buildActionPI(ctxt),
        IconCompat.createWithBitmap(bmp),
        ListBuilder.ICON_IMAGE, ctxt.getString(R.string.roll))
  }

  private fun buildActionPI(ctxt: Context): PendingIntent {
    val i = Intent(ctxt, SliceActionReceiver::class.java)

    return PendingIntent.getBroadcast(ctxt, 0, i, 0)
  }

  companion object {
    internal val FACES = intArrayOf(
        R.drawable.die_1,
        R.drawable.die_2,
        R.drawable.die_3,
        R.drawable.die_4,
        R.drawable.die_5,
        R.drawable.die_6
    )
    internal val ME = Uri.Builder()
        .scheme("content")
        .authority(BuildConfig.APPLICATION_ID + ".provider")
        .build()
  }
}
