/***
 * Copyright (c) 2018 CommonsWare, LLC
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain	a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 * by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
 * OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * Covered in detail in the book _The Busy Coder's Guide to Android Development_
 * https://commonsware.com/Android
 */

package com.commonsware.android.slice.sampler

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.IconCompat
import androidx.slice.Slice
import androidx.slice.SliceProvider
import androidx.slice.builders.*
import androidx.slice.core.SliceHints.ICON_IMAGE

class SamplerSliceProvider : SliceProvider() {
  override fun onCreateSliceProvider(): Boolean = true

  override fun onBindSlice(sliceUri: Uri): Slice? {
    val ctxt = context ?: return null

    return list(ctxt, sliceUri, ListBuilder.INFINITY) {
      setAccentColor(ResourcesCompat.getColor(ctxt.resources, R.color.colorAccent, null))
      addAction(buildIconAction(ctxt, "Top-level Action",
            R.drawable.ic_looks_two_black_24dp))

      header {
        title = "Header Title"
        subtitle = "This is the subtitle"

        setSummary("This is the summary", false)
      }

      row {
        title = "Simple Row Title"
        subtitle = "This is the subtitle"
        primaryAction = buildIconAction(ctxt, "Simple Row Primary Action",
                R.drawable.ic_looks_4_black_24dp)

        addEndItem(buildToggleAction(ctxt, "Simple Row End Item", R.id.toggle))
      }

      inputRange {
        title = "Range Title"
        subtitle = "This is the subtitle"
        max = 10
        value = 5
        inputAction = buildActionPI(ctxt, "Range Selection Changed", R.id.range)
      }
    }
  }

  private fun buildIconAction(ctxt: Context, msg: String, @DrawableRes iconRes: Int) =
      SliceAction.create(buildActionPI(ctxt, msg, iconRes),
          IconCompat.createWithResource(ctxt, iconRes), ICON_IMAGE, msg)

  private fun buildToggleAction(ctxt: Context, msg: String, id: Int) =
    SliceAction.createToggle(buildActionPI(ctxt, msg, id), msg, false)

  private fun buildActionPI(ctxt: Context, msg: String, id: Int): PendingIntent {
    val i = Intent(ctxt, SliceActionReceiver::class.java)
        .putExtra(SliceActionReceiver.EXTRA_MSG, msg)

    return PendingIntent.getBroadcast(ctxt, id, i, PendingIntent.FLAG_UPDATE_CURRENT)
  }
}
